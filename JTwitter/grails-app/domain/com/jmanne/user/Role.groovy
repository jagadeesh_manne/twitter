package com.jmanne.user

import com.jmanne.common.domain.BaseEntity

class Role extends BaseEntity{

	String authority

	static mapping = {
		cache true
	}

	static constraints = {
		authority blank: false, unique: true
	}
}
