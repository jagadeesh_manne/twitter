package com.jmanne.user

import com.jmanne.common.domain.BaseEntity;

class User extends BaseEntity {
	
	def slugGeneratorService;

	String displayName
	String email
	String password
	String slug;
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	static constraints = {
		displayName blank: false, size: 5..25
		email blank: false, unique: true, email: true
		password blank: false
	}

	static mapping = {
		password column: '`password`'
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role } as Set
	}

	def beforeInsert() {
		encodePassword()
		this.slug = slugGeneratorService.generateSlug(this.class, "slug", displayName)
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
		if (isDirty('displayName')) {
			this.slug = slugGeneratorService.generateSlug(this.class, "slug", displayName)
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}
}
