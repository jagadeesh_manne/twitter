package com.jmanne.twitter

import com.jmanne.common.domain.BaseEntity
import com.jmanne.user.User

class Tweet extends BaseEntity{
	
    String text;
	User user;
	
    static constraints = {
		text nullable:false, blank:false, maxSize: 150
    }
	
	static transients =["user"]
}
