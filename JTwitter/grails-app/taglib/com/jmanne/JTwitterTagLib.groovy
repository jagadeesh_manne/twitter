package com.jmanne


class JTwitterTagLib {
	
	static namespace = "jtwitter"
	
	def springSecurityService
	
	def username = { attrs ->
		if (springSecurityService.isLoggedIn()) {
			out << springSecurityService.getCurrentUser().displayName.encodeAsHTML()
		}
	}
}
