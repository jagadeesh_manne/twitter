package com.jmanne.twitter

import com.jmanne.BaseService
import com.jmanne.user.User

class TwitterService  extends BaseService{
	
	def save(Tweet tweet){
		tweet.user = springSecurityService.getCurrentUser();
		tweet.save(flush: true);
		log.info "User ${tweet.createdBy} is tweeted ${tweet.id}"
	}

	def findByUserId(Long userId, queryParams = [:]) {
		def offset = queryParams.offset ? queryParams.offset : 0;
		queryParams << [sort: "createdOn", order: "desc", offset: offset]
		def tweets = Tweet.findAllByCreatedBy(userId, queryParams)
		return injectUsers(tweets);
	}
	
	def findAll(queryParams = [:]) {
		def offset = queryParams.offset ? queryParams.offset : 0;
		queryParams << [sort: "createdOn", order: "desc", offset: offset]
		def tweets = Tweet.findAll(queryParams)
		return injectUsers(tweets);
	}
	
	def injectUsers(tweets) {
		def userIdSet = tweets*.createdBy
        def users = User.findAllByIdInList(userIdSet)
        def userMap = users.collectEntries {usr -> [(usr.id) : usr]}
        tweets.each { it ->
            it.user = userMap[it.createdBy]
        }
        return tweets
	}
}
