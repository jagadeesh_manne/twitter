package com.jmanne.user

import com.jmanne.BaseService

class UserService  extends BaseService{
	
	def save(User user){
		user.save(flush: true);
		log.info "User ${user.id} registered"
	}

	def get(Long userId){
		return User.get(userId);
	}
	
	def getBySlug(String slug){
		return User.findBySlug(slug);
	}
}
