modules = {
    application {
        resource url:'js/application.js'
		resource url:[dir: 'js', file:'jquery.js']
		resource url:[dir: 'js', file:'jquery.cuteTime.js']
		resource url:[dir: 'js', file:'bootstrap.js']
		resource url:[dir: 'js', file:'jtwitter.js']
		
		//CSS
		resource url:[dir: 'css', file:'main.css']
		resource url:[dir: 'css', file:'bootstrap.css']
		resource url:[dir: 'css', file:'jtwitter.css']
		
    }
}