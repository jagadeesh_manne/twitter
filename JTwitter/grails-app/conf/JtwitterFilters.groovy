import org.springframework.security.web.savedrequest.RequestCache

class JtwitterFilters {

	def grailsApplication;
	
	def filters = {
		all(controller: '*', action: '*') {
			before = {
				if(request.xhr) {
					def requestCache = 
						grailsApplication.getMainContext().getBean('requestCache') as RequestCache
					requestCache.removeRequest(request, response)
				}
			}

			after = { Map model ->
				
			}

			afterView = { Exception e ->
			}
		}
	}
}