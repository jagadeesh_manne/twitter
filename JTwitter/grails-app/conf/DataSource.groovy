import org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsAnnotationConfiguration

def dbName = appName.toLowerCase() // app name is set in config.groovy
def basePackage = "com.jmanne"
dataSource {
	configClass = GrailsAnnotationConfiguration.class
    driverClassName = "com.mysql.jdbc.Driver"
	username = "${dbName}"
	password = "${dbName}"
	dialect = "org.hibernate.dialect.MySQLDialect"
	url = "jdbc:mysql://localhost:3306/${dbName}?autoReconnect=true"
	
	pooled = true
	properties {
	   maxActive = -1
	   minEvictableIdleTimeMillis=1800000
	   timeBetweenEvictionRunsMillis=1800000
	   numTestsPerEvictionRun=3
	   testOnBorrow=true
	   testWhileIdle=true
	   testOnReturn=true
	   validationQuery="SELECT 1"
	}
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
        }
    }
    test {
        dataSource {
            dbCreate = "update"
        }
    }
    production {
       dataSource {
            username = "root"
           	password = ""
			url = "jdbc:mysql://localhost:3306/${dbName}?autoReconnect=true"
    		dbCreate = "update"
		}
    }
}
