package com.jmanne

class HomeController {

	def springSecurityService;
	
    def index() { 
		if (springSecurityService.isLoggedIn()) {
			redirect controller : "twitter", action:"index"
		}
		else {
			redirect controller: 'login'
		}
	}
}
