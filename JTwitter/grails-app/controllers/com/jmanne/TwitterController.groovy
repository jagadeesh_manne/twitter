package com.jmanne


import grails.converters.JSON
import grails.plugins.springsecurity.Secured

import com.jmanne.common.BaseController
import com.jmanne.twitter.Tweet

@Secured(['IS_AUTHENTICATED_FULLY'])
class TwitterController extends BaseController {

	def twitterService;
	
	def userService;
	
    def index() {
		def queryParams = [:]
		if(params.offset) {
			queryParams.offset = params.offset as int
		}
		def tweets = twitterService.findByUserId(loggedInUserId as Long);
		def tweet = new Tweet();
		render view : "/twitter/home" , model: [tweet:tweet, tweets:tweets];
	}
	
	def tweet() {
		def tweet = new Tweet(params)
		if(!tweet.validate()) {
			render tweet as JSON
		} else {
			twitterService.save(tweet)
			flash.message = message(code: 'tweet.success')
			render (template : "/twitter/tweet" , model : [tweet : tweet]) as JSON;	
		}	
	}
	
	
}
