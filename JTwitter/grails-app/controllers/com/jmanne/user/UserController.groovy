package com.jmanne.user

import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils

import com.jmanne.twitter.Tweet

class UserController {

	def userService;
	
	def twitterService;
	
    def signup() {
        def user = new User();
		render view: "signup", model : [user : user]
    }

    def register() {
        def user = new User(params)
		if(!user.validate()) {
			render view: "signup", model : [user : user]
		} else {
			userService.save(user)
			flash.message = message(code: 'signup.success')
			redirect(controller : "login", action: "index")
		}
    }
	
	def tweets() {
		def tweet = new Tweet(params)
		def user = userService.getBySlug(params.id)
		def tweets = twitterService.findByUserId(user.id);
		def config = SpringSecurityUtils.securityConfig
		String postUrl = "${request.contextPath}${config.apf.filterProcessesUrl}"
		render view : "/twitter/home" , model: [tweet:tweet, tweets:tweets, user:user,postUrl: postUrl,
		                           rememberMeParameter: config.rememberMe.parameter];
	}
}
