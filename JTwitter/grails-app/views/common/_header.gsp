<div class="navbar">
  <div class="navbar-inner">
	<a class="brand" href="#">JTwitter</a>
	<sec:ifLoggedIn>
	  	<ul class="pull-right toolbar">
	  	  <li>
	  	  	<i class="icon-user"></i><jtwitter:username/>
	  	  </li>
		  <li>
			<g:link controller='logout' >Logout</g:link>
		  </li>
		</ul>
	</sec:ifLoggedIn>
  </div>
  <div id="global-message" class="alert">
	
  </div>
</div>
