<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><g:layoutTitle default="JTwitter"/></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script>
    	var JTwitter = JTwitter || {};
    	JTwitter.contextPath = '${request.contextPath}'
    </script>
	<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
	<r:require modules="application"/>
	<g:layoutHead/>
    <r:layoutResources />
</head>
