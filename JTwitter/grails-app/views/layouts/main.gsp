<html>
<g:render template="/layouts/head"/>
<body>
	<div class="container">
		<g:render template="/common/header"/>
		<div id="body" class="body">
			<g:layoutBody/>
		</div>
	    <g:render template="/common/footer"/>
    </div>
</body>
</html>