<meta name='layout' content='main'/>
<g:if test="${user}">
<div class="alert alert-info">
	${user.displayName}'s Tweets
</div>
</g:if>

<div class="span4 side-bar-left">
	<div id="ajaxLoading" class="progress progress-striped active" style="display:none">
	  <div class="bar" style="width: 100%;"></div>
	</div>
	<div class='block'>
		<sec:ifLoggedIn>
			<g:render template="/twitter/form"/>
		</sec:ifLoggedIn>
		<sec:ifNotLoggedIn>
			<g:render template="/login/form"/>
		</sec:ifNotLoggedIn>
	</div>
</div>
<div class="span7">
	<g:render template="/twitter/tweets" model="['tweets':tweets]"/>
</div>