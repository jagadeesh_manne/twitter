<g:form url="[controller: 'twitter', action: 'tweet']"  name="tweet-form" id="tweetform">
	<div class="required">
		<span class="error">This Field required</span>
		<div class="control-group">
			<g:textArea name="text" id="tweet-feild" class="span4 required tweet-field" 
				placeholder="Whats on your mind" maxlength="150"/>
		</div>
	</div>
	<button name="publish" class="pull-right btn btn-info" id="tweet-btn">
		Publish
	</button>
	<span class="charLeft pull-right">150</span>
</g:form>