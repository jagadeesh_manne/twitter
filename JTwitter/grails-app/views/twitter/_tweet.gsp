<li class="content">
	<g:link controller="user" action="tweets" id="${tweet.user.slug}">
		<div class="header">
			<span class="by">
				<i class="icon-user"></i>${tweet.user.displayName}
			</span>
			<span class="prettyDate time pull-right">${tweet.createdOn}</span>
		</div>
		<p>${tweet.text}</p>
	</g:link>
</li>

