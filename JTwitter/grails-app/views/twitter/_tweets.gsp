<g:if test="${tweets}">
	<ul class="list-item tweets-list">
		<g:each in="${tweets}" status="i" var="tweet">
			<g:render template="/twitter/tweet" model="['tweet':tweet]"/>
		</g:each>
	</ul>
</g:if>
<g:else>
	<span class="emptyMsg"> You did not post any tweets</span>
	<ul class="list-item tweets-list"></ul>
</g:else>