<g:if test='${flash.message}'>
	<div class='hbox_message'>${flash.message}</div>
</g:if>

<form action='${postUrl}' method='POST' id='loginForm' class='cssform' autocomplete='off'>
	<p>
		<input type='text' class='text_' name='j_username' id='username' placeholder="Email"/>
	</p>

	<p>
		<input type='password' class='text_' name='j_password' id='password' placeholder="Password"/>
	</p>

	<p>
		<input type='submit' id="submit" class="btn btn-success" value='${message(code: "springSecurity.login.button")}'/>
	</p>
	<p>
		Dont have an account? 
		<g:link controller="user" action="signup">Signup here</g:link>
	</p>
</form>