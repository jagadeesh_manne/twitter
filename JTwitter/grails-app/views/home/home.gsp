<meta name='layout' content='main'/>
<div class="span4 side-bar-left">
	<div id="ajaxLoading" class="progress progress-striped active" style="display:none">
	  <div class="bar" style="width: 100%;"></div>
	</div>
	<div class='block'>
		<g:include view="/login/auth.gsp"/>
	</div>
</div>
<div class="span7">
	<g:render template="/twitter/tweets" model="['tweets':tweets]"/>
</div>