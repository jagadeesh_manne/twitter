<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12">
		  <div class="control-group">
		  	<g:hasErrors bean="${user}" field="displayName">
		  		<span class="error"><g:message code="${user.errors.getFieldError("displayName").code}"/></span>
			</g:hasErrors>
		    <div class="controls">
		      <g:textField name="displayName" value="${user.displayName}" placeholder="Full name" class="span12"/>
		    </div>
		  </div>
		  <div class="control-group">
		  	<g:hasErrors bean="${user}" field="email">
		  		<span class="error"><g:message code="${user.errors.getFieldError("email").code}"/></span>
			</g:hasErrors>
		    <div class="controls">
		      <g:textField name="email" value="${user.email}" placeholder="Email" class="span12"/>
		    </div>
		  </div>
		  <div class="control-group">
		  	<g:hasErrors bean="${user}" field="password">
		  		<span class="error"><g:message code="${user.errors.getFieldError("password").code}"/></span>
		  	</g:hasErrors>
		    <div class="controls">
		      <g:passwordField name="password" value="${user.password}" placeholder="Password" class="span12"/>
		    </div>
		  </div>
		  <div class="control-group">
		    <div class="controls">
				<g:submitButton value="Sign up" name="signup" class="btn btn-info" id="register-btn" />		    
			</div>
		  </div>
		 <div class="control-group">
		    <div class="controls">
				Already have an account? 
				<g:link controller="login" action="auth">sign in here</g:link>
			</div>
		  </div>
    </div>
  </div>
</div>