<meta name='layout' content='main'/>
<div id='hbox'>
	<div class='inner'>
		<h3 class="header">Register</h3>
		<g:form controller="user" action="register" name="signupForm" class='cssform' autocomplete='off'>
			<g:render template="/user/form"/>
		</g:form>
    </div>
</div>