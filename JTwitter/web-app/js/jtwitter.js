var JTwitter = JTwitter || {};
JTwitter.Api = {};

JTwitter = $.extend(JTwitter,{
	showLoader: function() {
		$('#ajaxLoading').show();
	},
	hideLoader: function() {
		$('#ajaxLoading').hide();
	},
	accessDenied: function() {
		window.location.href = JTwitter.contextPath + '/login';
	}
});

JTwitter.BootLoader = function() {
	$('#ajaxLoading').ajaxStart(JTwitter.showLoader);
	$('#ajaxLoading').ajaxComplete(JTwitter.hideLoader);
	$.ajaxSetup({'cache':false,
		error:function(event, jqXHR, ajaxSettings, thrownError){
			console.log('event',event)
			if(event.status == 401){
				window.location = JTwitter.contextPath + '/login';
			}
		}
	});
	$(JTwitter.Api.Tweet.init)
	$('.prettyDate').cuteTime({refresh: 1000});
};


$(JTwitter.BootLoader);

JTwitter.Api.Message = (function() {
	
	var timer ;
	function error(message) {
        showMessage(message, 'alert alert-error');
	}
	
    function success(message){
        showMessage(message, 'alert alert-success');
	}
    
    function showMessage(message, clazz) {
        if(timer) {
            clearTimeout(timer);
        }
		$('#global-message')
            .hide()
            .attr("class", clazz)
            .html(message)
            .show();
        timer = setTimeout('$(JTwitter.Api.Message.hide())', 5000);
    }
    
	function hide(){
		$('#global-message').fadeOut('slow');
	}
	
	return { 
		error : error, success: success, hide: hide
	}
})();

JTwitter.Api.Tweet = (function() {
	
	var EL = {
		iText : 'textarea[name=text]:input',
		submitButton: '#tweet-btn',
		tweetForm : '#tweetform',
	}
	
	var Errors = {
		length : 'Maximum characters exceed',
		empty  : 'This Field can not be blank'
	}
	
	function enableButton(){
		$(EL.submitButton).removeClass('disabled')
	}
	
	function disableButton(){
		$(EL.submitButton).addClass('disabled')
	}
	
	function showError(msg) {
		$(EL.tweetForm).find('.error').text(Errors[msg]).show();
		disableButton();
	}
	
	function hideError() {
		$(EL.tweetForm).find('.error').text('').hide();
		enableButton();
	}
	
	function init() {
		hideError();
		$(EL.iText).bind('keyup input change', function(e) {
			$(EL.tweetForm).find('.error').hide();
			var left = 150 - $.trim(this.value).length;
			var tweetLength = $.trim($(EL.iText).val()).length;
			$('.charLeft').css('color', (left > 0) ? 'green' : 'red');
			$('.charLeft').html(left);
			validate();
		})
		
		$(EL.submitButton).bind('click', function(e) {
			e.preventDefault();
			var valid = validate();
			if(valid) {
				publish();
			}
		})
	}
	
	function validate() {
		var tweetLength = $.trim($(EL.iText).val()).length;
		if(tweetLength === 0){
			showError('empty');
            return false
        } else if(tweetLength > 150){
        	showError('length');
            return false
        } else {
        	hideError();
        	return true;
        }
	}
	
	
	function publish() {
		jQuery.ajax({
			type:'POST',
			data:{text:$(EL.iText).val()}, 
			url: $(EL.tweetForm).attr('action'),
			success:function(data, textStatus, xhr){
				console.log('data', data)
				if(data.error === '401'){
					//JTwitter.accessDenied();
                } else {
					$(data).prependTo($('.tweets-list'));
					$(JTwitter.Api.Message.success('Tweet Published Successfully'));
					$(EL.iText).val('');
					$('.prettyDate').cuteTime({refresh: 1000});
					$('.charLeft').text(150);
					$('.emptyMsg').hide();
                }
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$(EL.iText).val('');
				$('.charLeft').text(150);
				if(jqXHR.status == '401') {
					$(JTwitter.Api.Message.error('Unauthorized Access.. Redirecting to Login page....'));
					setTimeout('$(JTwitter.accessDenied())', 2000);
				} else {
					$(JTwitter.Api.Message.error('Unknown error has occured please try again'));
				}
			}
		})
	}
	return { 
		init : init
	}
	
})();